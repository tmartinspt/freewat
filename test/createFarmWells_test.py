# -*- coding: utf-8 -*-
import sys
import os
import sqlite3
from tempfile import gettempdir
from qgis.core import *
from PyQt4.QtGui import QDialogButtonBox
from .utils import create_test_model
from ..dialogs.createFarmWells_dialog import CreateFarmWELayerDialog
from ..dialogs.createWELLayer_dialog import CreateWELayerDialog

from ..dialogs.createFarmId_dialog import createFarmIdDialog

app = QgsApplication(sys.argv, True)
QgsApplication.initQgis()

try:
    dlg = CreateFarmWELayerDialog(iface=None)
    assert(False) # should trigger an exception because model is missiing
except RuntimeError as e:
    assert(str(e).find('create a MODEL before') != -1)

dbfile = os.path.join(gettempdir(), 'mymodel.sqlite')
create_test_model(dbfile, app)

dlg = CreateWELayerDialog(iface=None)
dlg.textEdit.setText('well')
dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)

app.processEvents()

dlg = CreateFarmWELayerDialog(iface=None)
dlg.textEdit.setText('farm_well')

if len(sys.argv) > 1:
    dlg.show()
    app.exec_()
else:
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
    app.processEvents()

con = sqlite3.connect(dbfile)
cur = con.cursor()
cur.execute('select count(1) from farm_well_farm_well')
assert(cur.fetchone()[0]==100)
con.close()


# -*- coding: utf-8 -*-

import sys
import os
import sqlite3
from tempfile import gettempdir
from qgis.core import *
from PyQt4.QtGui import QDialogButtonBox
from .utils import create_test_model
from ..dialogs.createFarmPipeline_dialog import createFarmPipelineDialog

app = QgsApplication(sys.argv, True)
QgsApplication.initQgis()

try:
    dlg = createFarmPipelineDialog(iface=None)
    assert(False) # should trigger an exception because model is missiing
except RuntimeError as e:
    assert(str(e).find('create a MODEL before') != -1)

dbfile = os.path.join(gettempdir(), 'mymodel.sqlite')
create_test_model(dbfile, app)

dlg = createFarmPipelineDialog(iface=None)
dlg.textEdit.setText('pipeline')

if len(sys.argv) > 1:
    dlg.show()
    app.exec_()
else:
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
    app.processEvents()

con = sqlite3.connect(dbfile)
cur = con.cursor()
cur.execute("select count(1) from pipeline_div")
assert(cur.fetchone()[0]==100)
con.close()
#dlg = createFarmPipelineDialog(iface=None)

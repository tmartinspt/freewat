#
#coding=utf-8
#Copyright (C) 2015 Barcelona Science
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 Barcelona Science
:authors: L.M. de Vries, A. Nardi
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
import matplotlib
import matplotlib.pyplot as plt
from PyQt4 import QtGui
import sys

from ChemPlot import ChemPlot
from ChemPlot import CPSETTINGS

class SBDPlot(ChemPlot):
    '''
    This class implements the Schoeller Berkalof Plot.
    '''
    def __init__(self, parent=None):
        super(SBDPlot, self).__init__(parent)        
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_label_id, 'Schoeller Berkalof Plot')
        self.set_setting(CPSETTINGS.title, CPSETTINGS.title_y_position_id, 1.1)
        self.set_setting(CPSETTINGS.window, CPSETTINGS.window_title_id, 'Schoeller Berkalof')
        self.set_setting(CPSETTINGS.line_plot, CPSETTINGS.line_width_id, 2.0)
        
    def setData(self, labels, values):
        self.labels = labels        
        self.Cl = values['Cl']
        self.Ca = values['Ca']
        self.Mg = values['Mg']
        self.Na = values['Na']
        self.SO4 = values['SO4']
        self.HCO3 = values['HCO3']
        
        self.windowTitle = 'SBD Diagram'
        self.legendPosition = 'tightToXAxis'
        
    def pre_processing(self):
        #plt.xlabel(r'Electric Conductivity ($\mu$s/cm)', fontsize=16)
        plt.ylabel(r'meq/l', fontsize=16)

        self.ax.xaxis.tick_top()    
        self.ax.get_xaxis().set_tick_params(which='both', direction='out')
    
        self.ax.set_xticks([1,2,3,4,5,6])
        self.ax.set_xticklabels([ "Ca","Mg","Na","Cl","SO4","HCO3"])
        self.ax.set_xlim([0.5,6.5])
            
        self.ax.xaxis.grid(which='minor', color='0.65', linewidth=1, linestyle='--', alpha=1.0)
        
        self.ax.set_yscale('log')
        self.ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        self.ax.get_yaxis().set_tick_params(which='both', direction='out') 
        
        # or if you want differnet settings for the grids:                               
        self.ax.yaxis.grid(which='minor', alpha=0.6)                                                
        self.ax.yaxis.grid(which='major', alpha=0.8)        
        self.ax.grid()
    
    def post_processing(self):        
        pass
    
    def plot_values(self):
        for curPoint in range(len(self.labels)): 
            x = [1,2,3,4,5,6]
            y = [self.Ca[curPoint], self.Mg[curPoint], self.Na[curPoint], self.Cl[curPoint], self.SO4[curPoint], self.HCO3[curPoint]]
            label = self.labels[curPoint]
            self.private_plot(x, y, label, curPoint)

def plotSBD1(): 
    plot = SBDPlot()    
    labels = ["Well_1", "Well_2", "Well_3", "Well_4"]    
    Ca = [1.5, 4.5, 3.5, 0.35]
    Mg = [1.5, 4.5, 4.5, 1.5]
    Na = [1.5, 4.5, 3.5, 0.5]
    Cl = [1.5, 4.5, 2.5, 2.5]    
    SO4 = [1.5, 4.5, 0.5, 2.5]
    HCO3 = [1.5, 4.5, 3.5, 3.5]
    values = {'Ca':Ca, 'Mg':Mg, 'Na':Na, 'Cl':Cl, 'SO4':SO4, 'HCO3':HCO3}      
    plot.setData(labels, values)
    plot.draw()
        
if __name__ == '__main__':    
    app = QtGui.QApplication(sys.argv)
    plotSBD1()
    sys.exit(app.exec_())  
        
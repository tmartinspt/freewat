# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=AkvaGIS
qgisMinimumVersion=2.8
description=The AkvaGIS plugin enhances QGIS with hydrochemical and hydrogeological data processing and analysis. All reference and measurement data is stored in a Spatialite database.
version=1.0.0
author= IDAEA-CSIC
email=

# End of mandatory metadata

# Optional items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=geochemistry, hydrogeology, geoogy, hydrology, chemistry

homepage=
tracker=
repository=
icon=icons/akvaGIS.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

about=The plugin provides several tools for generating hydrogeochemical plots (Piper, SAR, SBD, Stiff, TimePlot), reports (Ionic Balance) and parameter maps (Stiff, Normative). Furthermore, it provides data exports for different geochemical tools (EasyQuim, StatQuimet and ExcelMIX) developed by IDAEA-CSIC. All reference and measurement data is stored in a Spatialite database.



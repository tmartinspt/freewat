
class InputCreator:

    def __init__(self, path, name, dictOfParams):
        self.modePath = path
        self.modelName = name
        self.params = dictOfParams

        self.createMPN()
        self.createMPBAS()
        self.createMPSIM()

    def createMPN(self):
        #DIS und Budget should be present (.cbc .dis .hds)
        lines = [
            'MPBAS 12 ' + self.modelName + '.mpbas',
            'DIS 13 ' + self.modelName + '.dis',
            'BUDGET 20 ' + self.modelName + '.cbc',
            'HEAD 22 ' + self.modelName + '.bhd'
        ]
        self.write(self.modePath + '.mpn', self.lineByLineWriter, lines)

    def createMPBAS(self):
        lines = [

        ]
        self.write(self.modePath + '.mpbas', self.lineByLineWriter, lines)

    def processParams(self):
        outstring = ''
        #1,2 oder 3 (endpoint, path, time series)
        if self.params['SimulationType'] == 'Endpoints':
            outstring += '1 '
        if self.params['SimulationType'] == 'Pathlines':
            outstring += '2 '
        if self.params['SimulationType'] == 'Timeseries':
            outstring += '3 '

        if self.params['TrackingDirection'] == 'Forward':
            outstring += '1 '
        if self.params['TrackingDirection'] == 'Backward':
            outstring += '2 '

        #WeakSinkOption
        outstring += '1 '
        #WeakSouceOption
        outstring += '1 '
        #ReferenceTimeOption
        outstring += '1 '
        #StopOption
        outstring += '1 '
        #ParticleGenerationOption (2 = read from external file)
        outstring += '2 '
        #TimePointOption
        outstring += '2 '
        #BudgetOutputOption
        outstring += '1 '
        #ZoneArrayOption
        outstring += '1 '
        #Retardation
        if bool(self.params['Retardation']):
            outstring += '2 '
        else:
            outstring += '1 '

        #AdvectiveObservationsOption
        outstring += '1 '
        return outstring

    def getReferenceTime(self):
        return self.params['ReferenceTime']

    def createMPSIM(self):
        lines = [
            self.modelName + '.mpn',
            self.modelName + '.mplst',
            #  SimulationType, TrackingDirection,
            #  WeakSinkOption, WeakSouceOption,
            #  ReferenceTimeOption, StopOption,
            #  ParticleGenerationOption,
            #  TimePointOption, BudgetOutputOption,
            #  ZoneArrayOption, RetardationOption
            # , AdvectiveObservationsOption
            self.processParams(),
            self.modelName + '.end',
            self.modelName + '.path',
            # ReferenceTime
            self.getReferenceTime(),
            self.modelName + '.strt'
        ]
        self.write(self.modePath + '.mpsim', self.lineByLineWriter, lines)

    def lineByLineWriter(self, file, listOfLines):
        for line in listOfLines:
            file.write(line + '\n')

    def write(self, path_to_file, writer, data):
        with open(path_to_file, 'w+') as f:
            writer(f, data)